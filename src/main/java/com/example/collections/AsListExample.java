package com.example.collections;


import java.util.*;

public class AsListExample {

    public static void main(String[] args) {
        String[] array = new String[] {"a", "b", "c"};
        List<String> asList = Arrays.asList(array);
        System.out.println("List:" + asList);
        System.out.println("Array:" + Arrays.toString(array));
        array[0] = "z";
        System.out.println("List:" + asList);
        System.out.println("Array:" + Arrays.toString(array));

        asList.set(0, "x");

        System.out.println("List:" + asList);
        System.out.println("Array:" + Arrays.toString(array));
    }
}
